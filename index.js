const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const dotenv = require('dotenv').config()


// Routes
const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');



// Connect to Express

let app = express();
let port = 3001;

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Connect to Mongoose

mongoose.connect(`mongodb+srv://jazz456:${process.env.PASSWORD}@cluster0.brhlyjl.mongodb.net/ecommerce-api?retryWrites=true&w=majority`, 
	{
		useNewUrlParser : true,
		useUnifiedTopology: true

	}
);


let db = mongoose.connection;
db.on('error', console.error.bind(console, "connection error"));

db.on('open', () => console.log("Connected to MongoDB"));


// Routes
app.use('/users', userRoutes);
app.use('/products', productRoutes);


// Server Listening
app.listen(port, () => console.log(`API is now online on port ${port}`));

