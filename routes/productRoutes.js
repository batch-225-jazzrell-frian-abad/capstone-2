const express = require('express');
const router = express.Router();
const auth = require('../auth'); //used to verify the bearer token first

const productController = require('../controllers/productController');



// =====================================================
// ADMIN ACCESS
// =====================================================

router.post('/addProduct', auth.verify, (req, res) => {

	const data = {
		product : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	productController.addProduct(data).then(result => res.send(result))
})


// Routes for Archiving 
router.put('/archive/:productId', auth.verify, (req, res) =>{

	const data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	productController.archiveProduct(data, req.params).then(result => res.send(result));
});


// Routes for Unarchiving
router.put('/unarchive/:productId', auth.verify, (req, res) =>{

	const data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	productController.unarchiveProduct(data, req.params).then(result => res.send(result));
});



router.get('/getAllProduct', auth.verify, (req, res) => {

	const data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}


	productController.getAllProducts(data).then(result => 
        res.send(result));
});



// Update Product Information
router.put('/:productId', auth.verify, (req, res) => {

	const data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}


	productController.updateProduct(data, req.params, req.body).then( result => res.send(result));
});




// ==============================================
// USER ACCESS
// ==============================================

router.get('/getActive', auth.verify, (req, res) => {

    productController.getAllActive().then(result => 
        res.send(result));
});


// put the ID in the req.body to search for specific product 
router.post('/getProduct', (req, res) => {
	productController.getProduct(req.body).then(result => res.send(result));
});








module.exports = router;