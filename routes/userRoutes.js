const express = require('express');
const router = express.Router();
const auth = require('../auth');
const Order = require('../models/order');

const userController = require('../controllers/userController');


router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(result => res.send(result));
})

router.post('/login', (req, res) =>{
	
	userController.loginUser(req.body).then(result => res.send(result));
})




router.post('/details', auth.verify, (req, res) => {
	userController.getProfile(req.body).then(result => res.send(result));
});


// SET USER TO ADMIN AND VICE VERSA - ADMIN ONLY
router.put('/userAuth/:userId', auth.verify, (req, res) =>{

	const data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	userController.userAuth(data, req.params, req.body).then(result => res.send(result));
});



router.post('/allDetails', auth.verify, (req, res) => {

	const data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	userController.getAllProfile(data).then(result => res.send(result));
});


// GET THE ORDER DETAILS VIA USER ID
// USER CHECKOUT ROUTES
router.post('/viewCart', auth.verify, (req, res) => {

	let data = ({

		// user ID will be retrieved from the request header
		userId : auth.decode(req.headers.authorization).id


	})

	userController.viewCart(data).then(result => res.send(result));
});







// GET ALL ORDERS ADMIN USER
router.get('/getAllOrders', auth.verify, (req, res) => {

	const data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}


	userController.getAllOrders(data).then(result => 
        res.send(result));
});








router.post('/addToCart', auth.verify, (req, res) => {

	let data = ({

		// user ID will be retrieved from the request header
		userId : auth.decode(req.headers.authorization).id,

		// Course ID will be retrieved from the request body
		productId : req.body.productId,
		//quantity : req.body.quantity

	})

	userController.addToCart(data).then(result => res.send(result));

	
});


router.post('/deleteToCart', auth.verify, (req, res) => {

	let data = ({

		// user ID will be retrieved from the request header
		userId : auth.decode(req.headers.authorization).id,

		// Course ID will be retrieved from the request body
		productId : req.body.productId,
		quantity : req.body.quantity

	})

	userController.deleteToCart(data).then(result => res.send(result));

	
});




router.post('/orderDetails', auth.verify, (req, res) => {

	let data = ({
		// user ID will be retrieved from the request header
		userId : auth.decode(req.headers.authorization).id

	})

	userController.getOrder(data).then(result => res.send(result));
});














router.post('/buynow', auth.verify, (req, res) => {

	let data = ({

		// user ID will be retrieved from the request header
		userId : auth.decode(req.headers.authorization).id,

		// Course ID will be retrieved from the request body
		productId : req.body.productId,
		quantity : req.body.quantity

	})

	userController.buyNow(data).then(result => res.send(result));

	
});




router.post('/checkoutTest', auth.verify, (req, res) => {

	let data = ({

		// user ID will be retrieved from the request header
		userId : auth.decode(req.headers.authorization).id,

		// Course ID will be retrieved from the request body
		productId : req.body.productId,
		quantity : req.body.quantity

	})

	userController.checkOutCart(data).then(result => res.send(result));

	
});














module.exports = router;