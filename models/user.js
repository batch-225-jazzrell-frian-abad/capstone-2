const mongoose = require('mongoose');


const userSchema = new mongoose.Schema({

	email: {
		type: String,
		required: [true, "email is required"]
	},
	password: {
		type: String,
		required: [true, "password is required"]
	},
	isAdmin : {
		type: Boolean,
		default : false
	}

});

// "module.exports" is the way for Node JS to treat this value as "package" that can be use to other files.


module.exports = mongoose.model('User', userSchema);