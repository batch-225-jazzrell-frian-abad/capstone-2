const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId : {
				type: String,
				required: [true, "User ID is required"]
	},
	products: [
		{
			productId : {
				type: String,
				required: [true, "Product ID is required"]
			},
			name : {
				type: String,
			},

			description : {
				type: String,
			},

			price : {
			type : Number,
			
			},

			quantity : {
				type: Number,
			},

			subtotal : {
				type: Number,
				default : 0
			},
			
			
			

		}

	],

	totalAmount : {
		type : Number,
		default : 0
	},

	purchasedOn : {
				type: Date,
				default: new Date()
	},

	isComplete : {
				type: Boolean,
				default: false
			}


	
})

module.exports = mongoose.model("Order", orderSchema);