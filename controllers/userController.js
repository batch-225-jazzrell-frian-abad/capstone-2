const User = require('../models/user');
const bcrypt = require('bcrypt');
const auth = require('../auth');
const Product = require('../models/product');
const Order = require('../models/order');
const OrderDetails = require('../models/orderDetails');


// User Registration
module.exports.registerUser = (reqBody) => {

	return User.findOne({ email: reqBody.email })
	.then(user => {
		if (user) {
			return { message: "Email already exists" };
		} else {
			let newUser = new User({
				email: reqBody.email,
				password: bcrypt.hashSync(reqBody.password, 10)
			});

			return newUser.save().then((user, error) => {
				if (error) {
					return false;
				} else {
					return { message: "User registered successfully", user };
				};
			});
		}
	})
};

// User Authentication - Login User and verify if existing user

module.exports.loginUser = (reqBody) =>{

	return User.findOne({email : reqBody.email}).then(result =>{

		if (result == null) {
			return {
				message: "User doest not Exist"
			}

		} else{

			const isPasswordCorrect = bcrypt.compareSync(
				reqBody.password, result.password);
			if (isPasswordCorrect) {
				if (result.isAdmin){

					return {
						message: "Welcome Admin",
						access: auth.createAccessToken(result)
					}
				} else {
					return {
						message: `Welcome ${result.email}`,
						access: auth.createAccessToken(result)
					}

				}
				
			} else {

				// if password doesn't match
				return {
					message: "Password was Incorrect"
				}
			};
		};
	});

};






// Get User Details for User

module.exports.getProfile = (userID, newContent) => {

	return User.findById(userID).then(result => {

		return result;
		
	})
}


// Get All user Details - for Admin Only
module.exports.getAllProfile = (data) => {
	
	if(data.isAdmin){
		return User.find({}).then(result => {

			return result;

		})
	}

	let message = Promise.resolve('User must be Admin to Access this.')

	return message.then((value) => {
		return value

	})



}

// Set User to Admin and Vice Versa - Admin Only

module.exports.userAuth = (data, reqParams, reqBody) => {

	if(data.isAdmin){

		return User.findById(reqParams.userId)
	.then((user, error) => {   // Return the value by Params.ID first
		if (!user) {
			return false;
		}
		user.isAdmin = reqBody.isAdmin; // to edit the isAdmin using reqBody
		return user.save(); // to save the value to the database
	})
	.then(user => {     // to get the value of isAdmin after saving and show results like removed:granted
		if (!user) {
			return false;
		}
		if (user.isAdmin) {
			return { message: "Admin access has been granted !!" };
		} else {
			return { message: "Admin access has been removed !!" };
		}
	})
	if (error) {
		return false;
	};
}
let message = Promise.resolve('User must be Admin to Access this.')

return message.then((value) => {
	return value
})

};





// Get all Order Details - Admin Only

module.exports.getAllOrders = (data) => {

	if(data.isAdmin){

		return Order.find({}).then((orders, error) => {
			if (error) {
				return false;
			} else {
				const filteredOrders = orders.filter(order => order.products.length > 0);
				return { "message": "List of Orders - Admin Only","Orders": filteredOrders };
			}
		});

	}

	let message = Promise.resolve('User must be Admin to Access this.')

	return message.then((value) => {
		return value

	})

};











// =================================================================
// Add to Cart
// =================================================================
// To Add a Product and transfer it to order.products 

module.exports.addToCart = async (data) =>{

	let product = await Product.find({})

	let order = await Order.findOne({ userId: data.userId });
	// if order object is empty create a new one
	if (!order) {
		order = new Order({
			userId: data.userId,
			products: []
		});
	}

	// to check the index of order.products
	let productIndex = order.products.findIndex(product => product.productId == data.productId);

	// if index is not -1 then run the condition
	/*if(productIndex !== -1) {
		// add the quantity base on the assigned quantity on the req.body.quantity
		order.products[productIndex].quantity += data.quantity;

		// to get the value of price * quantity
		let selectedProduct = product.find(p => p._id == data.productId);
		let subtotal = selectedProduct.price * order.products[productIndex].quantity;

		// to save the result of the computation to order.products[].subtotal
		order.products[productIndex].subtotal = subtotal;

	} else {*/

	let selectedProduct = product.find(p => p._id == data.productId);
	let subtotal = selectedProduct.price;

	order.products.push({ 
		productId: data.productId,
		quantity: data.quantity,
		price: selectedProduct.price,
		name : selectedProduct.name,
		subtotal : subtotal,
		description : selectedProduct.description
	});
	


	// function adds the "subtotal" property of the current value to the accumulator.
	/*let totalAmount = order.products.reduce((acc, curr) => acc + curr.subtotal, 0);

	order.totalAmount = totalAmount.toFixed(2);*/

	return order.save().then((order, error) => {
		if (error) {
			return false;
		} else {
			return { "message": "Your Cart has been Updated", "Data": order.products };
		};
	});

};










// Delete the product using the productID and quantity in the req.body

// Delete by ProductId
module.exports.deleteToCart = async (data) =>{
	
	let product = await Product.find({})

	let order = await Order.findOne({ userId: data.userId });


	// if order is empty return message
	if(!order || order.products.length == 0) {

		return {

			message: "Your Cart is Empty"
		}
	}

	// get the index of order.products to use for detection
	let productIndex = order.products.findIndex(product => product.productId == data.productId);


	// to check if the product Id correct before deletion if not show message
	if(productIndex == -1) {

		return {

			message: "Item not found for deletion"
		}

	}


	if (order.products[productIndex].productId = data.productId) {

		order.products.splice(productIndex, 1);

	}

	// to save changes
	return order.save().then((order, error) => {
		if (error) {
			return false;
		} else {

			if(!order || order.products.length == 0) {

				return 	{

					message: "Your Cart is Empty"
				}
			}
		}

		return { "message": "Your Cart has been Updated", "Cart": order.products };

	});

};





// for User View Cart function
module.exports.viewCart = async (data) => {

	let order = await Order.findOne({ userId: data.userId });

	return Order.find({ userId: data.userId, isComplete: false }).then((orders, error) => {
		if (error) {
			return false;
		} else {

			

			if(!order || order.products.length == 0) {

				return {

					message: "Your Cart is Empty"
				}
			}


			const filteredOrders = orders.filter(order => order.products.length > 0);


			return {  "Data": filteredOrders };
		}
	});
}




// Checkout the product you wanted via product ID
module.exports.checkOutCart = async (data) =>{

	let product = await Product.find({})

	let order = await Order.findOne({ userId: data.userId });

	// get the index of order.products to use for detection
	let productIndex = order.products.findIndex(product => product.productId == data.productId);


	// to check if the product Id correct before deletion if not show message
	if(productIndex == -1) {

		return {

			message: "Item not found for checkout"
		}

	}



	let removedProduct = []

	if (order.products[productIndex].quantity = data.quantity) {
		let selectedProduct = product.find(p => p._id == data.productId);
		let subtotal = selectedProduct.price * data.quantity;

		removedProduct = order.products.splice(productIndex, 1);
		removedProduct[0].subtotal = subtotal;
	}


	let selectedProduct = product.find(p => p._id == data.productId);
	let subtotal = selectedProduct.price * data.quantity;


		// reduce to iterate through an array 
	//let totalAmount = removedProduct.products.reduce((acc, curr) => acc + curr.subtotal, 0);

	// transfer value of totalAmount to order.totalAmount
	//total = totalAmount.toFixed(2);	

	let total = 0

	total += subtotal
	

	// to save changes
	return order.save().then((order, error) => {
		if (error) {
			return false;
		} 

		let newOrder = new Order({
			userId: order.userId,
			products: removedProduct,
			subtotal : subtotal,
			totalAmount: total,
			isComplete : true
		});

		newOrder.save().then((newOrder, error) => {
			if(error) {
				return false;
			} 

		});

		return { "message": "Your Item has been Ordered", "Cart": newOrder };


	});

};









// To Buy Single Product Only
module.exports.buyNow = async (data) =>{
	let order = await Order.findOne({ userId : data.userId });
	let product = await Product.findOne({_id: data.productId});


	let newOrder = new Order({
		userId: data.userId,
		products: [{
			productId: data.productId,
			quantity: data.quantity,
			subtotal: (product.price * data.quantity).toFixed(2),
			name: product.name,
			description: product.description,
		}],
		totalAmount: (product.price * data.quantity).toFixed(2),
		isComplete : true
	});

	return newOrder.save().then((order, error) => {
		if (error) {
			return {
				"message": "Error creating new order",
				"data": error
			};
		} else {
			return { "message": "Your Order has been Processed Successfully, Thank you !!" };
		}
	});
};






























































// 
module.exports.getOrder = async (data) => {
	return Order.find({ userId: data.userId, isComplete : true }).then((orders, error) => {
		if (error) {
			return false;
		} else {
			const filteredOrders = orders.filter(order => order.totalAmount > 0 && order.products.length > 0);
			return {  "Data": filteredOrders };
		}
	});
}
