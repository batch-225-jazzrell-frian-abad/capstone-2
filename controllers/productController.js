const Product = require('../models/product');
const auth = require('../auth')



// =================================================
// ADMIN ONLY ACCESS
// =================================================

// To Add Product on the Product List
module.exports.addProduct =(data) => {
	if(data.isAdmin){
		let new_product = new Product ({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		})

		return new_product.save().then((new_product, error) =>{

			if (error){
				return error
			}
			return {
				message: 'New Product successfully created!'
			}
		})
	}
	
	let message = Promise.resolve('User must be Admin to Access this.')

  return message.then((value) => {
    return value
  })

}


// To Set Product isActive to false for Archiving Method - Admin Only
module.exports.archiveProduct = (data, reqParams) =>{

  if (data.isAdmin) {
    let updateActiveField = {
      isActive: false
    };

    return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) =>{

      if (error){

        return false;

      } else {

        return {

          message: "Archiving Product succesfully"

        }

      };
    });
  }
  let message = Promise.resolve('User must be Admin to Access this.')

  return message.then((value) => {
    return value

  })

};

// To Set Product isActive to true for UnArchiving Method - Admin Only
module.exports.unarchiveProduct = (data, reqParams) =>{

  if (data.isAdmin) {
    let updateActiveField = {
      isActive: true
    };

    return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) =>{

      if (error){

        return false;

      } else {

        return {
          message: "Unarchiving Product succesfully"
        }

      };
    });
  }

  let message = Promise.resolve('User must be Admin to Access this.')

  return message.then((value) => {
    return value

  })


};



// To get all the product regardless if isActive true or false
module.exports.getAllProducts = (data) => {

  if(data.isAdmin){

    return Product.find({}).then(result => {
      return result
    });

  }

  let message = Promise.resolve('User must be Admin to Access this.')

  return message.then((value) => {
    return value

  })

};



// To Update product Admin Only
module.exports.updateProduct = (data, reqParams, reqBody) => {

  // specify the fields/properties of the document to be updated

  // to update data no need to put "new"
  if(data.isAdmin){

    let updatedProduct = {
      name: reqBody.name,
      description: reqBody.description,
      price: reqBody.price
    };

    return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {

      if (error) {

        return false;

      } else {

        return {
          message: "Product updated successfully"
        }

      };


    });

  }


  let message = Promise.resolve('User must be Admin to Access this.')

  return message.then((value) => {
    return value


  })

};















// =====================================================
// FOR USER 
// =====================================================

// getting all the product that isActive:true
module.exports.getAllActive = () => {

  return Product.find({isActive : true}).then(result => {
    return result
  });

};


// to get single product by using the ID of the product via req.body
module.exports.getProduct = (productID, re) => {

	return Product.findById(productID).then(result => {

		return result;
		
	})
}